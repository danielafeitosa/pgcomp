#!/usr/bin/env ruby

require 'time'
require "csv"
require "yaml"

OUTPUT_PATH = "#{Dir.pwd}/tmp/release_attributes"
TAGS_FILE = "#{OUTPUT_PATH}/tags"
METRICS_FOLDER = "#{OUTPUT_PATH}/metrics"

def collect_data
  puts "Collecting data..."

  CSV.open("#{OUTPUT_PATH}/metrics_data.csv", "a") do |metrics_csv|
    metrics_csv << ["Version", "codeLOC", "flog score", "average complexity per method"]
  end

  CSV.open("#{OUTPUT_PATH}/attributes_data.csv", "a") do |attr_csv|
    attr_csv << ["Version", "Number of commits", "Number of authors", "Authors", "Days between releases"]
  end

  TAGS.each_index do |n|
    version = TAGS[n]
    puts "======== #{version} ========"

    collect_attributes n
    collect_metrics version
  end
  `git checkout master`
end

def collect_attributes n
   number_of_commits = count_commits n

   authors = authors_numbers(n)
   number_of_authors = authors.size
   authors = authors.join(', ')

   days = time_between_releases n

   # commiters names and count

  CSV.open("#{OUTPUT_PATH}/attributes_data.csv", "a") do |attr_csv|
    attr_csv << [TAGS[n], number_of_commits, number_of_authors, authors, days]
  end
end

def count_commits n
  puts " * counting commits..."
  n_commits = if n.zero?
    `git rev-list --count #{TAGS[n]} --no-merges`
  else
    `git rev-list --count #{TAGS[n-1]}..#{TAGS[n]} --no-merges`
  end.chomp
end

def authors_numbers n
  puts " * counting authors..."
  authors = if n.zero?
    `git log --format='%aN' #{TAGS[n]} --no-merges | sort -u`
  else
    `git log --format='%aN' #{TAGS[n-1]}..#{TAGS[n]} --no-merges | sort -u`
  end.split("\n")
end

def time_between_releases n
  puts " * counting days between releases..."
  n_days = if n.zero?
    first_commit = Time.parse (`git log --format=%ad --reverse | head -1`).chomp
    first_tag = Time.parse (`git log #{TAGS[n]} -n 1 --format=%ad`).chomp
    (first_tag.to_date - first_commit.to_date).to_i
  else
    (release_date(TAGS[n]) - release_date(TAGS[n-1])).to_i
  end
end

def release_date version
  time = Time.parse (`git log #{version} -n 1 --format=%ad`).chomp
  time.to_date
end

def collect_metrics version
  puts " * collecting metrics..."
  output_file = "#{METRICS_FOLDER}/#{version}.yml"
  `git checkout --quiet #{version}`
  `metric_fu --format yaml --out '#{output_file}'`

  CSV.open("#{OUTPUT_PATH}/metrics_data.csv", "a") do |metrics_csv|
    file = YAML.load_file(output_file)
    metrics_csv << [version, file[:stats][:codeLOC], file[:flog][:average], complexity_average(file)]
  end
end

def complexity_average output
  methods_complexity = []
  output[:saikuro][:files].each do |file|
    file[:classes].each do |klass|
      klass[:methods].each do |method|
        methods_complexity << method[:complexity]
      end
    end
  end
  methods_complexity.inject(:+).to_f / methods_complexity.length
end

puts "Running..."

if File.file?(TAGS_FILE)
  puts "tags file already exists... The tags listed in that file will be used"
else
  system "git tag --sort=v:refname > #{TAGS_FILE}"
end

TAGS = File.readlines(TAGS_FILE).map {|line| line.strip}

collect_data
