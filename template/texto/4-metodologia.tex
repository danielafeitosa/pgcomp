\chapter{Metodologia} \label{chapter:metodologia}

% A metodologia deve trazer de forma explícita uma proposta para a seqüência de
% atividades (etapas) necessárias para o cumprimento dos objetivos. É importante
% buscar uma íntima relação entre o cronograma de atividades propostas e a
% metodologia. Ou seja, enquanto o cronograma traz títulos de atividades e prazos,
% sem maiores detalhamentos, a metodologia deve traduzir “como” cada um dos itens
% do cronograma será realizado. Tal associação permite um melhor dimensionamento
% do tempo associado a cada atividade e favorece a análise de exeqüibilidade de
% cada um dos itens propostos.

Neste capítulo será apresentada a metodologia utilizada na pesquisa.

Esse estudo pretende investigar as seguintes hipóteses em relação à evolução
de um projeto de software livre:

\begin{description}
  \item[H1:] {\it Os valores das métricas de código-fonte se comportam como
distribuições estatísticas} \hfill \\
  A identificação de padrões nas métricas de código-fonte ao longo da vida de
um software permitirá entender a variação dos valores das métricas e a criação
de um modelo de monitoramento
  \item[H2:] {\it A evolução das métricas dos plugins do software não seguem o
mesmo modelo das métricas do core} \hfill \\
  Nos softwares que seguem esse tipo de organização, os métodos estatísticos
serão aplicados separadamente nos dados obtidos a partir da parte central do
código ({\it core}) do software e {\it plugins}. Os modelos obtidos serão
comparados para compreender a relação entre o crescimento do {\it core} e
crescimento de {\it plugins}
  \item[H3:] {\it A variação das métricas de código-fonte em versões de manutenção é
menor que nas versões com novas funcionalidade} \hfill \\
  Versões de manutenção de um software normalmente são utilizadas para resolver
pequenos problemas ou erros ({\it bugs}) e não devem adicionar novas
funcionalidades ou conteúdo. Nos softwares que disponibilizam os dois tipos de
versões (manutenção e novas funcionalidades) espera-se que a variação das
métricas seja menor entre as versões de manutenção
\end{description}

Para a conclusão dos objetivos deste trabalho e validação das hipóteses, as
seguintes atividades serão realizadas:

\section{Revisão de literatura}

Será realizado um levantamento e análise de estudos sobre os assuntos abordados
na dissertação: evolução de software, métricas de código-fonte, utilização e
acompanhamento de métricas no desenvolvimento de software livre, modelos de
qualidade de software livre.

\section{Planejamento do estudo exploratório}

Será realizado um planejamento detalhado do estudo, incluindo definição das
métricas que serão coletadas, as características que serão utilizadas de cada
versão, os métodos e modelos estatísticos que serão aplicados.

\subsection{Seleção das métricas} \label{subsection:planejmetricas}

Para a seleção inicial das métricas a serem analisadas, foram utilizados os
resultados obtidos sobre quais métricas devem ser controladas ao longo do tempo
no estudo sobre métricas de código-fonte em projetos de software
livre\cite{meirelles2013monitoramento}. Nesse estudo, Meirelles apresentou
como analisar e monitorar essas métricas para obtenção dos valores
frequentes em um determinado software. Os critérios utilizados pelo autor para a
escolha das métricas foram baseados (i) nos resultados a partir dos trabalhos
realizados em conjunto com grupos do projeto QualiPSo e (ii) na existência de
estudos que sugerem valores teóricos de referência.

\begin{itemize}
  \item Métricas de tamanho
  \begin{itemize}
    \item LOC ({\it Lines of Code}): número de linhas de código
    \item AMLOC ({\it Average Method LOC}): média do número de linhas de código
por método
    \item {\it Total Number of Modules or Classes}: número total de módulos ou
classes
  \end{itemize}
  \item Indicadores estruturais
  \begin{itemize}
    \item NOA ({\it Number of Attributes}): número de atributos de uma classe
    \item NOM ({\it Number of Methods}): número de métodos de uma classe
    \item NPA ({\it Number of Public Attributes}): número de atributos públicos
    \item NPM ({\it Number of Public Methods}): número de métodos públicos
    \item ANPM ({\it Average Number of Parameters per Method}): média do número de parâmetros por método
    \item DIT ({\it Depth of Inheritance Tree}): profundidade da árvore de herança
    \item NOC ({\it Number of Children}): número total de filhos de uma classe
    \item RFC ({\it Response For a Class}): respostas para uma classe
    \item ACCM ({\it Average Cyclomatic Complexity per Method}): média da complexidade ciclomática por método
  \end{itemize}
  \item Métricas de acoplamento
  \begin{itemize}
    \item ACC ({\it Afferent Connections per Class}): mede a conectividade de
uma classe. Se uma classe referencia muitas outras classes, o valor dessa
métrica ficará alto e indicará que uma alteração na classe provavelmente afetará
outras classes
    \item CBO ({\it Coupling Between Objects}): mede o acoplamento entre
objetos. Se uma classe é muito referenciada por outras classes, o valor dessa
métrica ficará alto e indicará que uma alteração na classe provavelmente afetará
outras classes
    \item COF ({\it Coupling Factor}): indica o quão interconectado é o
software. O valor dessa métrica é a razão entre o total de ligações entre as
classes e o total possível de ligações. Um valor alto nessa métrica indica que
um baixo grau de independência entre os módulos, dificultando a legibilidade e
manutenção do software.
  \end{itemize}
  \item Métricas de coesão
  \begin{itemize}
    \item LCOM ({\it Lack of Cohesion in Methods}): mede ausência de coesão em
métodos. Hitz e Montazeri\cite{hitz1995measuring} revisaram a definição inicial
da LCOM e a nomearam como LCOM4. Por essa definição, o cálculo dessa métrica é
realizado a partir da construção de um gráfico em que os nós são os métodos e
atributos de uma classe e as arestas conectam cada método com outros métodos ou
variáveis que ele utiliza. O valor da LCOM4 é o número de componentes fracamente
conectados no grafo. Como coesão é uma propriedade desejável, o valor ideal
dessa métrica é 1.
    \item SC ({\it Structural Complexity}): mede a complexidade estrutural,
calculada a partir do produto de acoplamento (CBO) e coesão
(LCOM4)\cite{darcy2005structural}.
  \end{itemize}
\end{itemize}

\subsection{Seleção das características} \label{subsection:planejcaracteristicas}

Algumas características das versões variam ao longo da vida do software e podem ser
identificadas a partir do código-fonte e das informações do repositório
(mensagens de {\it commits}, por exemplo). Essas características podem
influenciar os valores das métricas do software. Inicialmente, as informações que
serão coletadas para utilização no estudo são as listadas abaixo:

\begin{description}
  \item[Fatores humanos] {Pessoas que incorporaram código na árvore oficial {\it commiters} em
determinada versão, pessoas que alteraram código em determinada versão e suas
características:} \hfill \\
 Os indivíduos envolvidos no desenvolvimento de uma versão
afetam a qualidade do software desenvolvido e por isso precisam ser considerados
para a definição do mo modelo de monitoramento de um software. Estudos afirmam
que a qualidade do software é influenciado pela produtividade, capacidade,
experiência, grau de autoria e nível de participação (centrais ou
periféricos) dos desenvolvedores do projeto\cite{banker1991model,
darcy2005structural, beaver2006effects, gorla2010determinants,
matsumoto2010analysis, decaracterizaccao, fernandez2011influence}.
  \item[Tipo de lançamento] Em muitos projetos de software livre as versões
lançadas são classificadas como versões com novas funcionalidades ou versões de
manutenção: \hfill \\
 As versões de manutenção são utilizadas em alguns projetos para correção de
lançamento de versões de manutenção e geralmente incluem apenas correções de
pequenos defeitos {\it bugs} ou melhorias e é realizado no período entre os
lançamentos de versões com novas funcionalidade e por não incluir grandes
mudanças,  espera-se que essas versões intermediárias não alterem as métricas de
qualidade de software de forma significativa
  \item[Tempo entre o lançamento de versões:] o tempo entre os lançamentos pode
ser significativo na variação das métricas do código-fonte. \hfill \\
  O tempo mais longo entre os lançamentos, com maior período de testes, pode
resultar em valores de métricas mais interessantes para o projeto.
\end{description}

\subsection{Seleção da ferramenta de análise estática de código-fonte}
\label{subsection:planejferramenta}

O software escolhido para o estudo é o Noosfero\footnote{http://noosfero.org},
um plataforma livre para criação de ambientes colaborativos para gestão e
produção de conhecimento e de economia solidária. Entre as possibilidades de
utilização dessa plataforma, podemos citar a implementação de redes sociais,
intranets, ambientes virtuais de ensino e aprendizagem e {\it e-commerce}. O Noosfero é
licenciado sob a GNU Affero General Public License
(AGPL)\footnote{http://www.gnu.org/licenses/agpl.html}, versão 3 e desenvolvido
na linguagem Ruby, utilizando o {\it framework}
Rails\footnote{http://www.rubyonrails.org}.

Para a escolha da ferramenta para análise estática de código-fonte desse
estudo, serão considerados os fatores (i) disponibilidade sob uma licença de
software livre (ii) mantido por uma comunidade ativa de desenvolvedores (iii)
suporte à linguagem do Noosfero (iv) realização de coleta das métricas
utilizadas no estudo. Caso não seja encontrada uma ferramenta que atenda a todos
os requisitos, selecionaremos a mais próxima da desejada e será desenvolvida
uma proposta de alteração da ferramenta para utilização no estudo.

\section{Coleta de dados} \label{section:coleta}

Nesta etapa serão coletados os dados necessários para o estudo. Inicialmente
serão analisadas as versões do software livre Noosfero lançadas entre 12/09/2007
e 20/11/2015. São 158 versões, sendo 109 versões de manutenção e 49 versões com
novas funcionalidades.

\subsection{Coleta de métricas de código-fonte} \label{subsection:coletametricas}

As métricas selecionadas na etapa de planejamento do estudo
(subseção~\ref{subsection:planejmetricas}) serão coletadas a partir dos dados das
versões do Noosfero. Para a extração das métricas de código-fonte será utilizada
a ferramenta automatizada escolhida na
subseção~\ref{subsection:planejferramenta}). As métricas do {\it core} e dos
{\it plugins} serão calculadas separadamente.

\subsection{Coleta das características das versões do software}
\label{subsection:coletacaracteristicas}

Todas as características selecionadas na
subseção~\ref{subsection:planejcaracteristicas} deverão ser obtidas a partir do
código-fonte e das informações do repositório. Para a coleta dessas informações
será utilizada uma ferramenta automatizada ({\it script}) que extrairá de cada
versão analisada as características de interesse do estudo.

\section{Análise de dados} \label{section:analise}

Inicialmente os dados coletados (subseções~\ref{subsection:coletametricas} e
~\ref{subsection:coletacaracteristicas}) serão classificados de acordo com os
tipos de dados (contínuo, ordinal ou nominal), o tipo distribuição em relação ao
tempo e os tipos das amostras (variáveis dependentes ou independentes). A
análise dos dados será realizada a partir da aplicação de métodos estatísticos
sobre os dados para a identificação dos relacionamentos entre as variáveis.

Serão utilizadas técnicas de análise exploratória de dados multivariados para
identificar as características importantes do conjunto de dados. A utilização
dessas técnicas auxilia (i) na detecção de anomalias e {\it outliers} nos dados
(ii) na identificação dos relacionamento entre as variáveis (iii) a evidenciar
informações implícitas dos dados\cite{tukey1977exploratory}. Como sugerido por
Tukey, para cada componente analisado serão realizadas análises exploratórias
dos dados univariada antes de executar a análise multivariada.

Para a realização dessa etapa será necessário o suporte de especialistas em
estatística para a escolha do teste estatístico apropriado e acompanhamento das
análises.

Com essa análise, pretende-se obter as correlações entre as métricas do
código-fonte e entre as métricas e as características das versões ao longo da
evolução do software.

\section{Interpretação dos resultados} \label{section:interpretacao}

As correlações encontradas na análise dos dados (seção~\ref{section:analise})
permitir\~{a}o a compreensão do comportamento de uma variável em relação às outras. A
partir desse entendimento, será possível verificar como uma variável é afetada
pela alteração das outras variáveis, servindo como base para a definição de um
modelo de monitoramento do software.

\section{Replicação do estudo em outro software}

Caso seja possível a definição do modelo de monitoramento a partir das informações
obtidas com a caracterização, as etapas seguidas nesse estudo serão aplicadas em
outro software livre para verificar se o processo de definição do modelo está
bem definido e se pode ser generalizado para aplicação em outros softwares.

Se não for possível a replicação desse estudo em outro software por limitação de
tempo, essa etapa será sugerida na seção trabalhos futuros.

\section{Escrita da dissertação}

A escrita da dissertação será realizada paralelamente à execução das etapas
descritas na metodologia.
