\chapter{Fundamentação Teórica} \label{chapter:fundamentacao}

% Fundamentação Teórica e Revisão do Estado da Arte
% Citar as pesquisas correlatas que foram recentemente desenvolvidas e estão
% também em desenvolvimento. Referenciar os principais trabalhos científicos
% relativos à proposta, situando claramente o estado da arte e buscando
% referências atualizadas e coerentes com o proposto. Procurar detalhar aspectos
% teóricos ainda não cobertos pela literatura, identificando possíveis brechas
% ainda não exploradas e sua relação com a proposição.

\section{Evolução de Software}

Evolução de software é o processo pelo qual se dá a realização de mudanças em um
software para contemplar a necessidade de novos requisitos, correção de defeitos
{\it bugs} apresentados em versões anteriores ou melhorias de atributos de
qualidade. É necessário que sistemas de software ativos passem por processos contínuos de
evolução para evitar a degradação do software e manter a utilidade do sistema.

A evolução do software pode melhorar ou deteriorar a qualidade do software.
Neste caso, a qualidade pode ser medida a partir de atributos externos (percebidos pelos usuários, como desempenho, disponibilidade, usabilidade) ou
atributos internos (percebidos a partir do código-fonte, como métricas, legibilidade, reusabilidade, etc).

Segundo Lehman~\cite{lehman1980laws}, sistemas {\it E-type} são sistemas de software que
resolvem um problema ou implementam uma solução de um domínio do mundo real.
Essa definição inclui a maioria dos sistemas conhecidos e utilizados. 
%Nesses tipos de sistema, um atributo de qualidade relevante é a satisfação dos usuários.
Como as necessidades do mundo real estão inseridas em um processo de mudanças contínuas,
software do tipo {\it E-type} precisa refletir essas mudanças
para se adaptar ao ambiente que está inserido, sem comprometer sua qualidade e sua
utilidade. 

\begin{table}[htb]
  \begin{tabular}{|p{1cm}|p{6cm}|p{9cm}|}
     \hline
     \textbf{Nº Lei} & \textbf{Nome} & \textbf{Descrição} \\ \hline
     I    & Mudança contínua & Sistemas {\it E-type} devem ser continuamente
adaptados, senão se tornam progressivamente menos satisfatórios \\ \hline
     II   & Complexidade crescente & À medida que um sistema {\it E-type}
evolui, sua complexidade aumenta a menos que seja realizado um trabalho para
mantê-la ou reduzí-la. \\ \hline
     VI  & Crescimento contínuo & As funcionalidades de sistemas devem ser
melhoradas continuamente para manter a satisfação dos usuários durante a vida do
sistema \\ \hline
     VII  & Qualidade decrescente & A menos que rigorosamente adaptado para
considerar as mudanças em seu ambiente operacional, a qualidade de um sistema
vai aparentar estar diminuindo enquanto ele evolui \\ \hline
   \end{tabular}
   \caption{Leis I, II, VI e VII  de Evolução de Software - Leis de Lehman}
   \label{leislehman}
\end{table}

Por outro lado, o envelhecimento de um software~\cite{Parnas:1994:SA:257734.257788} é caracterizado pela perda de
desempenho, número crescente de defeitos {\it bugs} causados por alterações
inadequadas na sua estrutura e consequente perda de usuários por não mais atender às
suas necessidades. Entender as causas do envelhecimento de software é
importante para tomar medidas para minimizar seus efeitos ou reverter seus
danos.

Para ajudar a entender e caracterizar o processo de evolução de sistemas {\it E-type} 
e retardar seu envelhecimento, possivelmente aumentando o seu tempo de vida, 
Lehman formulou suas oito leis,  conhecidas como ``Leis de Evolução de Lehman''. 
As Leis de Lehman evidenciam a necessidade de estudos sobre evolução de software. 
No contexto deste trabalho, destacaremos as leis I, II, VI e VII (Tabela~\ref{leislehman}),
possivelmente usadas para justificar algumas dimensões de análise ao longo da pesquisa.


\section{Métricas de código-fonte}

Os valores das métricas de código-fonte representam alguma característica do
software analisado. Algumas métricas podem ser utilizadas para quantificar o
tamanho,  indicadores estruturais, medidas de acoplamento e coesão.
Individualmente, os valores das métricas podem não fornecer informações
relevantes sobre o software mas, quando combinadas com outras métricas ou
fatores, podem auxiliar no desenvolvimento e melhoria do código-fonte.
Considerando os valores da métricas e a interpretação de suas combinações, os
responsáveis pelo projeto de software podem tomar decisões sobre partes do código-fonte que
precisam ser refatorados, por exemplo.

\subsection{Acompanhamento de métricas em desenvolvimento de software livre}

As métricas relacionadas ao histórico de um software (sensíveis à história)
avaliam as características do software ao longo da sua vida e fornecem indicadores
sobre a evolução do código-fonte, como complexidade, acoplamento,
coesão~\cite{da2010detecting}.

Anquetil \textit{et al}~\cite{anquetilevoluccao} analisaram a evolução de complexidade de
três sistemas no contexto da segunda lei de Lehman, que afirma que a
complexidade do sistema aumenta à medida que o sistema evolui. Foram analisados
três versões de cada um dos sistemas e coletadas as métricas relacionadas à complexidade
(i) linhas de código (ii) linhas de comentários (iii) linhas de
``lixo de código'' (códigos não executados, que deveria ter sido excluídos) (iv)
número de rotinas (métodos) (v) complexidade ciclomática (vi) complexidade
estrutural de Card e Glass (vii) complexidade de dados de Card e Glass (viii)
complexidade de sistema de Card e Glass. 
Na análise, as métricas foram agrupadas em ``Métricas de tamanho e compreensibilidade'' 
e ``Métricas de complexidade''. 
Os resultados encontrados mostram que a complexidade aumentou ao
se consider o aumento nos valores das métricas relacionadas ao tamanho do software e
diminuiu, considerando o aumento de compreensibilidade. 
Em relação às métricas de complexidade, os resultados mostram que os sistemas seguem 
padrões diferentes em alguns aspectos e não foi possível confirmar o aumento da complexidade 
para o sistema inteiro. Portanto, os fatores que influenciam a variação da
complexidade observada em um sistema não são equivalentes aos fatores que
influenciam na variação de outros sistemas.

Mockus \textit{et al}~\cite{mockus2002two} analisaram o histórico de alterações no
código-fonte e os registros de defeitos ({\it bugs}) dos projetos Apache e Mozilla
considerando a participação e papéis dos desenvolvedores, tamanho da equipe do
{\it core}, densidade de defeitos e tempo de resolução de defeitos. Os
resultados da análise foram comparados a 3 diferentes projetos comerciais
proprietários e mostram que a densidade de defeitos dos projetos Apache e
Mozilla são menores que as dos outros 3 projetos.

Trung e Bieman~\cite{dinh2005freebsd} repetiram o estudo de caso de Mockus \textit{et al},
analisando o projeto FreeBsd e verificaram que quando comparadas apenas versões
de funcionalidades novas com os projetos comerciais proprietários, então as
densidades de defeitos dos projetos são equivalentes.

Com base nos estudos de Mockus \textit{et al} e Trung e Bieman, percebemos que quando
analisamos o histórico da variação de complexidade de um software precisamos
considerar o tipo de lançamento da versão, isto é, se é uma versão de novas
funcionalidades ou de manutenção.

Terceiro e Chavez~\cite{terceiro2009structural} realizaram um estudo de caso
sobre a evolução da complexidade estrutural em 21 versões de um projeto de
software livre escrito na linguagem C. Os resultados mostram que existe
correlação linear entre o lançamento e complexidade e sugerem que esse aumento
está relacionado com as mudanças arquiteturais realizadas em cada versão.

Terceiro~\cite{decaracterizaccao} analisou o histórico de mudanças realizadas em
13 sistemas escritos em diferentes linguagens de programação. O estudo tinha o
objetivo de identificar a contribuição de alguns fatores, como experiência dos
desenvolvedores e sua familiaridade com o sistema, variação no tamanho,
espalhamento das mudanças e maturidade do processo de desenvolvimento para a
variação da complexidade estrutural e os efeitos da complexidade estrutural
sobre os projetos. Os resultados sugerem que é possível identificar os períodos
de mudanças da arquitetura do sistema a partir das variações abruptas na
complexidade estrutura do sistema. Além disso, mostra que o papel que o
desenvolvedor representa (central ou periférico) no projeto influencia na
alteração complexidade no código-fonte. Segundo o estudo, os desenvolvedores
centrais introduzem menos complexidade  e causam maiores reduções na
complexidade estrutural que os desenvolvedores periféricos.

Meirelles~\cite{meirelles2013monitoramento} avaliou a distribuição e correlações
dos valores das métricas de 38 projetos de software livre e discutiu as relações
de causalidade e implicações práticas-gerenciais para monitoramento das mesmas.
Como contribuição do estudo, foram observados os tipos de distribuição que governam
15 métricas utilizadas na avaliação dos projetos e, a partir das análises
estatísticas da correlação entre as métricas, foi definido um subconjunto de
métricas para serem monitoradas ao longo do tempo.

Neste trabalho de mestrado serão utilizadas as métricas sugeridas por Meirelles~\cite{meirelles2013monitoramento} para realizar a
caracterização da evolução de um projeto de Software Livre.

\section{Modelos de qualidade}

A qualidade de um produto de Software Livre pode ser afetada por diversos
fatores e depende do produto analisado. Número de usuários, tempo de
projeto, frequência de lançamento, número de defeitos {\it bugs}, métricas de
código-fonte e disponibilidade de documentação são alguns atributos que podem
ser utilizados como indicadores de qualidade.

Stol \textit{et al}~\cite{stol2010comparison} realizaram uma revisão sistemática para
identificação de métodos de avaliação de qualidade de {\it FLOSS (Free/Libre and
Open Source Software)} e apresentaram um framework para comparação dos métodos. Os
modelos de qualidade encontrados no estudo são realizados a partir de requisitos
não-funcionais, então a avaliação depende da percepção que os {\it stakeholders}
têm sobre as características do software. Esse modelos são importantes quando é necessário 
 fazer comparação entre produtos de software.

Garvin~\cite{garvin1984does} e Kitchenham e Pfleeger~\cite{kitchenham1996software}
listaram visões sobre qualidade, consideradas importantes, tais como:
\begin{itemize}
\item visão do usuário, com foco na adequação às necessidades dos usuários;
\item visão do processo, com foco na conformidade com a especificação e capacidade organizacional de
desenvolver o produto seguindo o processo de software; e
\item visão do produto, considerando que as características dos produtos são definidas pelas
características de seus subprodutos.
\end{itemize}

Neste trabalho, será utilizada a visão do produto, para análise das características do
software para a definição do modelo de monitoramento específico do software
analisado.


