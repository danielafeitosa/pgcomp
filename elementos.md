-- Experimentation in Software Engineering
# Problemática

(...)

# Problema

(...)

# Hipóteses

* O aumento de tempo entre o lançamento de versões diminui a quantidade de bugs encontrados na versão
* O aumento de tempo entre o lançamento de versões diminui a porcentagem de aumento da complexidade
* A abertura do core para escrita de mais commiter diminui a porcentagem de aumento da complexidade

/* Perguntas que podem ser feitas para o projeto e que podem ser respondidas */
/* A entrada de novos commiters como pode afetar? */

(...)

# Objeto:

Código das versões de um software livre

# Objetivo:

Estudar a evolução de um projeto a partir da análise das tags do software

/* Caracterizar a partir de / com o objetivo de / - Olhar GQM template */
/* estudar fenômenos/características que mudam ao longo do tempo */

Caracterizar a evolução de um projeto de software livre ao longo do seu tempo de vida

object of study 	evolução de um projeto de software livre
purpose 	caracterizar
focus 	complexidade do software
stakeholder 	release manager
context factors 	segunda lei de Lehmann

O objetivo desse estudo é caracterizar a variação da complexidade durante a evolução de um projeto de software livre do ponto de vista do release manager com o objetivo de compreender a relação com (i) aumento de tempo entre o lançamento de versões e (ii) abertura para novos commiters.


* O aumento de tempo entre o lançamento de versões diminui a quantidade de bugs encontrados na versão
* O aumento de tempo entre o lançamento de versões diminui a porcentagem de aumento da complexidade
* A abertura do core para escrita de mais commiter diminui a porcentagem de aumento da complexidade

caracterizar: descrever com exatidão

Esta tese apresenta uma caracterizac~ao da complexidade estrutural em sistemas de soft-
ware livre, com objetivo de identicar (i) a contribuic~ao de diversos fatores para a variac~ao
da complexidade estrutural e (ii) os efeitos da complexidade estrutural sobre projetos de
software.

The purpose of this study is to characterize the effect of pair programming on programmer effort and program quality from the point of view of software managers in the context of a small web-development company.

#Objetivos específicos:


* Caracterizar a relação entre quantidade de commiters e aumento de complexidade do
* software
* Estudar a evolução de um projeto a partir da análise das tags do software
  * Métricas de complexidade
  * Metricas de manutenibilidade
* Estudar a relação entre o tempo entre os lançamentos e quantidade de bugs
* encontrados (verificado a partir da quantidade de versões de manutenção
* lançadas)
* Estudar a relação entre o tempo entre os lançamentos e a evolução da
* complexidade do software


# Metodologia

* Revisão sistemática da literatura sobre utilizações e acompanhamento de métricas
no desenvolvimento de software livre
* Coleta de métricas a partir dos dados das versões de um software livre
*

(...)

# Resultados esperados


